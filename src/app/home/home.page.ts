import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    bottles = [1,2,3,4,5,6,7,8,9,10];
    bottle : number;

    times = [1,2,3,4,5,6,7,8];
    time : number;

    weight: number;
    gender: number;

    grams: number;
    gramsLeft: number;
    liters: number;

    burning: number;
    promilles: number;
    result: number;

   
  constructor() {}


  calculate(){

    this.liters = this.bottle * 0.33;
    this.grams = this.liters * 8 * 4.5;

    this.burning = this.weight / 10;
    this.gramsLeft = this.grams / (this.weight * this.time);

    this.result = this.grams / (this.weight * this.gender);

    this.promilles = this.result;

    //console.log(this.promilles, this.gender, this.weight, this.time, this.bottle);
    //console.log(this.grams, this.gramsLeft, this.liters, this.result);
  }

}
